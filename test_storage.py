#
# NOTE: This file is not intended to be modified during the exercise.
# It exists to illustrate (and test) the functionality of the BlockDevice class
#

from storage import BlockDevice


def test_total_blocks():
    # by default the block device has 1024 blocks
    device = BlockDevice.create()
    assert len(device) == 1024


def test_block_device_configuration():
    # the block device can be configured with different block sizes and counts
    device = BlockDevice.create(block_size=4, block_count=5)
    assert len(device) == 5
    for block in device.blocks:
        assert len(block) == 4


def test_block_write():
    # writing to a block device with 0% corruption rate
    # always returns the value stored in the referenced block
    value = 'example-example'
    device = BlockDevice.create(corruption_rate=0.0)
    written = device.write(0, value)
    assert written.startswith(value)
    assert device.read(0).startswith(value)


def test_block_write_truncation():
    # writing a value that's longer than the block size
    # automatically truncates trailing characters
    value = 'example-example-example'
    device = BlockDevice.create(corruption_rate=0.0)
    written = device.write(0, value)
    assert written == 'example-example-'
    assert device.read(0) == 'example-example-'

    # writing to an existing block does not replace the entire block content
    device.write(0, 'foo')
    assert device.read(0) == 'foo' + 'mple-example-'
    device.write(0, 'x')
    assert device.read(0) == 'x' + 'oomple-example-'


def test_data_corruption():
    value = 'example-example'
    device = BlockDevice.create(corruption_rate=1.0)

    # block devices with 100% corruption never return the return value
    # (they always return corrupted data)
    for _ in range(100):
        assert device.read(0) != device.read(0)
        written = device.write(0, value)
        assert not written.startswith(value)
