#
# NOTE: This file is not intended to be modified during the exercise.
# Rather, it is meant to be integrated with by implementing the
# KeyValueStore class defined in kv.py
#

from dataclasses import dataclass
import random
import secrets
import string


def random_str(count):
    return ''.join(
        secrets.choice(string.ascii_letters + string.digits)
        for i in range(count)
    )


@dataclass
class BlockDevice:
    block_size: int
    blocks: list[str]
    corruption_rate: float

    @staticmethod
    def create(
        block_size: int = 16,
        block_count: int = 1024,
        corruption_rate: float = 0.0
    ):
        return BlockDevice(
            block_size=block_size,
            blocks=[random_str(block_size) for _ in range(block_count)],
            corruption_rate=corruption_rate
        )

    def __len__(self):
        return len(self.blocks)

    def maybe_corrupt(self, index: int):
        if random.uniform(0, 1) < self.corruption_rate:
            self.blocks[index] = random_str(self.block_size)

    def read(self, index: int) -> str:
        self.maybe_corrupt(index)
        return self.blocks[index]

    def write(self, index: int, content: str) -> str:
        truncated = content[: self.block_size]
        current_content = self.blocks[index]
        self.blocks[index] = truncated + current_content[len(truncated) :]
        self.maybe_corrupt(index)
        return self.blocks[index]
