# Backend Interview Exercise

This exercise is a test drive to see how you work through a complex software engineering problem. Our scheduled interview may not be enough time to complete the entire project, and that's okay. We're interested in observing how you organize and spend your time.

This exercise is **open book** and is intended to be written in Python3.  If you need to reference something, feel free to ask questions or consult online documentation during the exercise.  Work with your interviewer to explain your thought process as you work, and if you're stuck, ask for help as you would a coworker.  We're not assessing how much you keep stored in your brain but instead how you think through problems and communicate with teammates to solve a problem.

## Preparing for your Test Drive Interview

Software engineers on our team aren't generally given a problem without context and asked to solve it in 90 minutes, which is why you've been provided the details of this exercise ahead of your Test Drive.

However, we ask that you refrain from writing any code/implementation ahead of time; our goal is to assess and discuss your approach as you implement a solution to this problem during our scheduled Test Drive call.

Before your interview, make sure you've got the necessary software installed for an interactive programming exercise.

1. Ensure that you have `python3` and `git` installed on your computer, along with an editor of your choice.

2. Clone this repository: `git clone https://gitlab.com/missioncloud-public/backend-interview-exercise.git`

3. Install a test runner library of your choosing (our team uses [py.test](https://pypi.org/project/pytest/)) e.g., `pip3 install pytest`.

4. Run the example provided test suite and observe that it fails.

## Key Value Store

During this exercise, we will be writing a class that implements a key-value store for ASCII content (https://gitlab.com/missioncloud-public/backend-interview-exercise/-/blob/main/kv.py)

```python
class KeyValueStore:

    def __init__(self):
        # Create a block device with 1024 blocks that can each fit a 16 character string;
        # every read/write has a 5% chance of corruption.
        self.device = BlockDevice.create(
            block_size=16,
            block_count=1024,
            corruption_rate=0.05
        )

    def set(self, key, value):
        raise NotImplementedError()

    def get(self, key):
        raise NotImplementedError()

    def delete(self, key):
        raise NotImplementedError()


kv = KeyValueStore()
kv.set('username', 'admin')
assert kv.get('username') == 'admin'
```

Internally, your class should use the provided `BlockDevice` class (https://gitlab.com/missioncloud-public/backend-interview-exercise/-/blob/main/storage.py), which provides functionality similar to a block device.

https://en.wikipedia.org/wiki/Block_(data_storage)

Just like in the real world, the block device you’ll be using is susceptible to data corruption:

 - When you read content from the device, the block’s content may be corrupted and return incorrect values.
 - When you write content to the device, the data written may be corrupted.

Your implementation should:

 - Run in-memory (the provided `BlockDevice` implementation doesn't perform any actual disk operations).
 - Implement the example `KeyValueStore` methods to support managing ASCII content using the block device for storage
 - Manage keys - and their mapping to ASCII values that are distributed across one or more blocks - using an in-memory data structure of your choosing (only the values themselves need to be stored in the block device).
 - Include a strategy for detecting data loss due to corruption
 - Not worry about efficiency (memory or otherwise) - focus primarily on building a correct program that can be observed runnning with passing unit tests

If time allows, be prepared to discuss how you might adapt your implementation to not only detect data loss, but mitigate/correct it.

Please optimize your implementation for:
 - Clean, explainable code
 - Correctness, including observable unit tests that run and pass
