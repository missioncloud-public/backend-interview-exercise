from kv import KeyValueStore


def test_kv_retrieval():
    kv = KeyValueStore()

    assert kv.get('example-key') is None
    kv.set('example-key', 'example-value')
    assert kv.get('example-key') == 'example-value'

    kv.delete('example-key')
    assert kv.get('example-key') is None
