from storage import BlockDevice


class KeyValueStore:

    def __init__(self):
        # Create a block device with 1024 blocks that can each fit a 16 character string;
        # every read/write has a 5% chance of corruption.
        self.device = BlockDevice.create(
            block_size=16,
            block_count=1024,
            corruption_rate=0.05
        )

    def set(self, key, value):
        pass

    def get(self, key):
        pass

    def delete(self, key):
        pass
